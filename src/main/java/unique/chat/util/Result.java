package unique.chat.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.util.EntityUtils;

public class Result {

    private String cookie;

    private int statusCode;

    private HashMap<String, Header> headerAll;

    private HttpEntity httpEntity;

    private String body;

    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public HashMap<String, Header> getHeaders() {
        return headerAll;
    }

    public void setHeaders(Header[] headers) {
        headerAll = new HashMap<String, Header>();
        for (Header header : headers) {
            headerAll.put(header.getName(), header);
        }
    }

    public HttpEntity getHttpEntity() {
        return httpEntity;
    }

    public void setHttpEntity(HttpEntity httpEntity) {
        this.httpEntity = httpEntity;
    }

    @SuppressWarnings("deprecation")
    public void setBody(InputStream content) {
        try {
            this.body = IOUtils.toString(content, EntityUtils.getContentCharSet(this.getHttpEntity()));
            IOUtils.closeQuietly(content);
        } catch (IOException e) {
            e.printStackTrace();
            this.body = "";
        }
    }

    public String getBody() {
        return this.body;
    }
}
