package unique.chat;


public interface IChat {

    String getMessage(String param);
    
    String getJson(String param);
    
    String getXml(String param);
}
