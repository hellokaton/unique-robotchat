package unique.chat.xiaodou;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.protocol.HTTP;

import unique.chat.IChat;
import unique.chat.util.HttpUtil;

public class XiaoDou implements IChat {

    static final String REQ_URL = "http://xiao.douqq.com/bot/chat.php";

    static final Map<String, String> params = new HashMap<String, String>();
    static final Map<String, String> header = new HashMap<String, String>();
    
    static{
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("User-Agent", "Mozilla/4.0 (compatible; MSIE 5.0; Windows NT; DigExt)");
        header.put("Referer", "http://xiao.douqq.com/");
    }
    
    @Override
    public String getMessage(String param) {
        params.put("chat", param);
        try {
            return removeAD(HttpUtil.post(REQ_URL, header, params, HTTP.UTF_8).getBody());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    private String removeAD(String sendMsgs) {
        if (sendMsgs.indexOf("xiaodouqqcom") != -1) {
            sendMsgs = "伦家不懂官人的话了啦~";
        } else if (sendMsgs.indexOf("simsimi2") != -1) {
            sendMsgs = "伦家不懂官人的话了啦~";
        } else if (sendMsgs.indexOf("douqq") != -1) {
            sendMsgs = "伦家不懂官人的话了啦~";
        }
        sendMsgs = sendMsgs.replaceAll("傻逼", "sb").replaceAll("小九", "毛小驴")
                .replaceAll("小豆", "毛小驴").replaceAll("小贱豆", "毛小驴")
                .replaceAll("小贱", "毛小驴");
        return sendMsgs;
    }

    @Override
    public String getJson(String param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getXml(String param) {
        // TODO Auto-generated method stub
        return null;
    }

}
