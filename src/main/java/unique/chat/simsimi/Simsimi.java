package unique.chat.simsimi;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.protocol.HTTP;

import unique.chat.IChat;
import unique.chat.util.HttpUtil;

public class Simsimi implements IChat {

    static final String REQ_URL = "http://www.niurenqushi.com/app/simsimi/ajax.aspx";

    static final Map<String, String> params = new HashMap<String, String>();

    static final Map<String, String> header = new HashMap<String, String>();

    static {
        header.put("Content-Type", "application/x-www-form-urlencoded");
        header.put("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:21.0) Gecko/20100101 Firefox/21.0");
        header.put("Accept", "*/*");
        header.put("Referer", "http://www.niurenqushi.com/app/simsimi/");
    }

    @Override
    public String getMessage(String param) {
        params.put("txt", param);
        try {
            return removeAD(HttpUtil.post(REQ_URL, header, params, HTTP.UTF_8).getBody());
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    // 去除广告
    private String removeAD(String sendMsgs) {
        if (sendMsgs.indexOf("simsimi2") != -1) {
            sendMsgs = "偶是毛小驴，女，还木有男友，欢迎南华学子调戏   O(∩_∩)O";
        } else if (sendMsgs.indexOf("Database") != -1 || sendMsgs.indexOf("Failed") != -1) {
            int random = (int) (Math.random() * 5);
            switch (random) {
            case 1:
                sendMsgs = "嗯";
                break;
            case 2:
                sendMsgs = "聊天其它的吧";
                break;
            case 3:
                sendMsgs = "嗯哼";
                break;
            case 4:
                sendMsgs = "哎呀";
                break;
            case 5:
                sendMsgs = "额";
                break;
            default:
                sendMsgs = "嗯";
                break;
            }
        } else if (sendMsgs.indexOf("Unauthorized access") != -1) {
            sendMsgs = "我怎么听不懂你说的什么意思呀[大哭]。咱们能换个话题吗！";
        } else if (sendMsgs.indexOf("你可以教我回答") != -1) {
            sendMsgs = "好吧";
        }
        // 替换部分内容
        sendMsgs = sendMsgs.replaceAll("傻逼", "sb");
        sendMsgs = sendMsgs.replaceAll("小九", "毛小驴");
        // sendMsgs = sendMsgs.replaceAll("小豆", "小贱贱");
        sendMsgs = sendMsgs.replaceAll("小豆机器人网页版地址：http://xiao.douqq.com QQ个性网http://www.xiugexing.com", "伦家不懂官人的话了啦~");
        sendMsgs = sendMsgs.replaceAll("小豆", "毛小驴");
        sendMsgs = sendMsgs.replaceAll("人家", "伦家");
        sendMsgs = sendMsgs.replaceAll("林晨爱你QQ个性网http://www.xiugexing.com", "伦家不懂官人的话了啦~");
        return sendMsgs;
    }

    @Override
    public String getJson(String param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getXml(String param) {
        // TODO Auto-generated method stub
        return null;
    }

}
